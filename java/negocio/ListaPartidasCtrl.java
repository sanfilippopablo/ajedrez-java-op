package negocio;

import datos.ColeccionPartidas;
import entidades.Jugador;
import entidades.Partida;
import errores.ValidationError;

public class ListaPartidasCtrl {

	public ColeccionPartidas coleccionPartidas = new ColeccionPartidas();
	public InicioCtrl inicioCtrl = new InicioCtrl();
	
	/*
	 * Funcion que busca una partida por id previamente guardada
	 * y lanza la partida
	 */
	public Partida iniciarPartida(Integer id) throws ValidationError{
		
		//Valida que el usuario haya seleccionado una partida
		if(id == null){
			throw new ValidationError("No selecciono partida.");
		}
		
		Partida p = coleccionPartidas.getPartidaById(id);
		
		// Validar que la partida no este vacia
		if(p == null){
			throw new ValidationError("No se ha encontrado partida.");
		}
		
		return p;
	}
	
	/*
	 * Inicia una nueva partida entre los dos jugadores
	 */
	public Integer iniciarNuevaPartida(Jugador j1, Jugador j2) {
		return coleccionPartidas.crearNuevaPartida(j1, j2);
	}
	

}
