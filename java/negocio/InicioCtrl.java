package negocio;

import java.util.ArrayList;

import datos.ColeccionJugadores;
import datos.ColeccionPartidas;
import entidades.Jugador;
import entidades.Partida;
import errores.ValidationError;

public class InicioCtrl {
	
	ColeccionPartidas coleccionPartidas = new ColeccionPartidas();
	ColeccionJugadores coleccionJugadores = new ColeccionJugadores();
	private ArrayList<Partida> partidas = new ArrayList<Partida>();
	private ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

	// Devuelve un arreglo de id de las partidas 
	// pasando 2 dni de jugadores
	public ArrayList<Partida> iniciar(String dniText1, String dniText2) throws ValidationError{
		
		ArrayList<Jugador> jugadores = getJugadores(dniText1, dniText2);		
		partidas = coleccionPartidas.getPartidas(jugadores.get(0).getdNI(),jugadores.get(1).getdNI());
		
		return partidas;
	}

	public ArrayList<Jugador> getJugadores(String dniText1, String dniText2) throws ValidationError{
		
		// Validar campos vacios
		if(dniText1.contentEquals("") | dniText2.contentEquals("")){
			throw new ValidationError("Uno de los dos está vacío.");
		}
				
				
		// Validar int
		int dni1, dni2;
		try {
			dni1 = Integer.parseInt(dniText1);
			dni2 = Integer.parseInt(dniText2);
		} catch (Exception e) {
			throw new ValidationError("Uno de los dos no es un entero.");
		}
		
		// Validar que no sean iguales
		if (dni1 == dni2) {
			throw new ValidationError("Los DNI son iguales.");
		}
	
		// Validar jugadores existentes
		Jugador j1 = coleccionJugadores.getJugadorByDNI(dni1);
		Jugador j2 = coleccionJugadores.getJugadorByDNI(dni2);

		if (j1 == null || j2 == null) {
			throw new ValidationError("Uno de los dos DNI no está en la DB.");
		}else{
			jugadores.add(j1);
			jugadores.add(j2);
		}
		return jugadores;
		
	}

}
