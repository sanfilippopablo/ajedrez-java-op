package negocio;

import datos.ColeccionPartidas;
import entidades.Partida;
import entidades.Pieza;
import errores.ValidationError;

public class JuegoCtrl {

	public void moverPieza(Partida partida, String idPieza, String pX, String pY) throws Exception {
		int posX, posY;
		try {
			posX = Integer.parseInt(pX);
			posY = Integer.parseInt(pY);
		} catch (Exception e) {
			throw new ValidationError("Posición final no válida.");
		}
		Pieza pieza = partida.getPiezaById(idPieza);
		if(pieza == null){
			throw new ValidationError("No se encontro la pieza.");
		}
		
		partida.mover(pieza, posX, posY);
		
		
		// Eliminar partida si termina
		if (partida.estaTerminada()) {
			ColeccionPartidas cp = new ColeccionPartidas();
			
			cp.eliminarPartida(partida);
		}
	}

}
