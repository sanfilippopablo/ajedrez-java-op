package datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FabricaConexion {
	
	private String dbDriver = "com.mysql.jdbc.Driver";
	private String host = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	private String port = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	private String user = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
	private String pass = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
	private String db = "ajedrez";
	
	private Connection conn;
	private int cantCon;
	
	
	private FabricaConexion(){
		
		try {
			Class.forName(dbDriver);
			conn=null;
			cantCon=0;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static FabricaConexion instancia;
	
	public static FabricaConexion getInstancia(){
		if (instancia==null){
			instancia = new FabricaConexion();
		}
		return instancia;
	}
	
	
	
	public Connection getConn(){
		try {
			if(conn==null || conn.isClosed()){
				conn = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+db+"?user="+user+"&password="+pass+"&allowMultiQueries=true");
				cantCon++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	
	public void releaseConn(){
		try {
			cantCon--;
			if(cantCon==0){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
