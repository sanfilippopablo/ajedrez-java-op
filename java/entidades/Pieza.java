package entidades;

import errores.ValidationError;

public class Pieza {
	
	Jugador jugador;
	int posX;
	int posY;
	String id;
	
	public Pieza(int posX, int posY, Jugador jugador, String id) {
		this.posX = posX;
		this.posY = posY;
		this.jugador = jugador;
		this.id = id;
	}

	public Pieza() {
		// TODO Auto-generated constructor stub
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public void mover(int posXDestino, int posYDestino) throws ValidationError {
			setPosX(posXDestino);
			setPosY(posYDestino);
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		return true;
	}

	public String getId() {
		return id;
	}
	
}