package entidades;

public class Rey extends Pieza {
	
	public Rey(int posX, int posY, Jugador jugador, String id) {
		super(posX, posY, jugador,id);
	}

	public Rey() {
		// TODO Auto-generated constructor stub
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		int diffX = Math.abs(posXDestino - posX);
		int diffY = Math.abs(posYDestino - posY);
	
		return diffX <= 1 && diffY <= 1;
	}
}
