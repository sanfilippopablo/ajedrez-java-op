package entidades;

public class Torre extends Pieza {
	
	public Torre(int posX, int posY, Jugador jugador, String id) {
		super(posX, posY, jugador, id);
	}

	public Torre() {
		// TODO Auto-generated constructor stub
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		return posXDestino == posX || posYDestino == posY;
	}
}
