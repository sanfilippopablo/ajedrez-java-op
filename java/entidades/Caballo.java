package entidades;

public class Caballo extends Pieza {
	
	public Caballo(int posX, int posY, Jugador jugador, String id) {
		super(posX, posY, jugador,id);
	}
	
	public Caballo() {
		// TODO Auto-generated constructor stub
	}

	public boolean esMovimientoValido(int posXDestino, int posYDestino) {
		int diffX = Math.abs(posXDestino - posX);
		int diffY = Math.abs(posYDestino - posY);
		
		return (diffX == 2 && diffY == 1) || (diffX == 1 && diffY == 2);
	}

}
