package errores;

public class ValidationError extends Exception {

	private static final long serialVersionUID = 1L;

	public ValidationError() {
		super();
	}

	public ValidationError(String message) {
		super(message);
	}

}
