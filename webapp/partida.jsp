<%@ page errorPage="partidas.jsp" %>
<%@ page isErrorPage="true" import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page
	session="true"
	import="negocio.ListaPartidasCtrl"
	import="entidades.Partida"
	import="errores.ValidationError"
	import="java.util.ArrayList"
	import="entidades.Pieza"
%>
<%
	session = request.getSession();
	if(session.getAttribute("dni1") == null | session.getAttribute("dni2") == null){
		response.sendRedirect("index.jsp");
	}
	String idPartida = "";
	if(!request.getParameter("idPartida").isEmpty()){
		idPartida = request.getParameter("idPartida").toString();
	}
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Partida</title>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<div class="page-header">
				<h1 style="display: inline">Partida ID:<%= idPartida %> entre <%=(String)session.getAttribute("jugador1")%> y <%=(String)session.getAttribute("jugador2")%></h1>
			<a style="float: right" class="btn btn-default" href="cerrarSesion.jsp">Cerrar Sesión</a>
			</div>
<% 
		ListaPartidasCtrl listaPartidasCtrl = new ListaPartidasCtrl();
		Partida partida = null;
		int id;
		try {
			id = Integer.parseInt(idPartida);
		} catch (Exception e) {
			throw new ValidationError("ID de partida no es valido.");
		}
		
		partida = listaPartidasCtrl.iniciarPartida(id);
		
		if(partida == null){
			response.sendRedirect("partidas.jsp");	
		}
%>
		
		<div class="row">
			<div class="col-sm-4">
				<h3>Piezas de <%=(String)session.getAttribute("jugador1")%></h3>
				<table class="table">
					<thead>
						<tr>
							<th>Pieza</th>
							<th>Posición X</th>
							<th>Posición Y</th>
						</tr>
					</thead>
					<tbody>
<%		
		ArrayList<Pieza> piezas = partida.getPiezas();
		for(Pieza pieza: piezas) {
			if (pieza.getJugador().getdNI() == Integer.parseInt(session.getAttribute("dni1").toString())) {
%>
						<tr>
							<td>
								<%=pieza.getId() %>
							</td>
							<td>
								<%=pieza.getPosX() %>
							</td>
							<td>
								<%=pieza.getPosY() %>
							</td>
						</tr>	
<% 
			}
		}
%>
					</tbody>
				</table>
			</div>

			<div class="col-sm-4">
				<h3>Piezas de <%=(String)session.getAttribute("jugador2")%></h3>
				<table class="table">
					<thead>
						<tr>
							<th>Pieza</th>
							<th>Posición X</th>
							<th>Posición Y</th>
						</tr>
					</thead>
					<tbody>
<%		
		for(Pieza pieza: piezas) {
			if (pieza.getJugador().getdNI() == Integer.parseInt(session.getAttribute("dni2").toString())) {
%>
						<tr>
							<td>
								<%=pieza.getId() %>
							</td>
							<td>
								<%=pieza.getPosX() %>
							</td>
							<td>
								<%=pieza.getPosY() %>
							</td>
						</tr>	
<% 
			}
		}
%>
					</tbody>
				</table>
			</div>


		<div class="col-sm-4">
			<h3>Turno actual: <%=partida.getTurnoActual().getApellido()%></h3>

<% 			
			if(exception != null & exception instanceof ValidationError){
%>	
				<p style="color: red">
					<%=exception.getMessage() %>
				</p>	
<%
			} 
%>
				
				<form class="form-horizontal" action="mover.jsp" method="post">
					<div class="form-group control-label">
						<label class="col-sm-4">Pieza</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" name="pieza" />
						</div>
					</div>
					<div class="form-group control-label">
						<label class="col-sm-4">Posición X</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" name="posX" />
						</div>
					</div>
					<div class="form-group control-label">
						<label class="col-sm-4">Posición Y</label>
						<div class="col-sm-8">
							<input class="form-control" type="text" name="posY" />
						</div>
					</div>
					<input type="hidden" name="idPartida" value="<%=partida.getId()%>" />

					<div class="form-group control-label">
						<div class="col-sm-offset-4 col-sm-8">
							<input class="btn btn-primary" type="submit" value="Mover" />
						</div>
					</div>
				</form>
			</div>		
		</div>
		</div>

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</body>
</html>