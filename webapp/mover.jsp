<%@ page errorPage="partida.jsp" %>
<%
	session = request.getSession();
	if(session.getAttribute("dni1") == null | session.getAttribute("dni2") == null){
		response.sendRedirect("index.jsp");
	}
%>
<%@ page 
	session="true"
	import="negocio.JuegoCtrl"
	import="entidades.Partida"
	import="negocio.ListaPartidasCtrl"	
%>
<%
	String idPieza = request.getParameter("pieza").toString();
	String pX = request.getParameter("posX").toString();
	String pY = request.getParameter("posY").toString();
	String idPartida = request.getParameter("idPartida").toString();
	System.out.println(pX);
	System.out.println(pY);
	System.out.println(idPieza);
	System.out.println(idPartida);
	ListaPartidasCtrl listaPartidasCtrl = new ListaPartidasCtrl();
	int id = Integer.parseInt(idPartida);
	Partida partida = listaPartidasCtrl.iniciarPartida(id);
	
	JuegoCtrl juegoCtrl = new JuegoCtrl();
	juegoCtrl.moverPieza(partida, idPieza, pX, pY);
	if(partida.estaTerminada()){
		partida.cambiarTurno();
		session.setAttribute("ganador", partida.getTurnoActual().getApellido()+" "+partida.getTurnoActual().getNombre());
		response.sendRedirect("ganolapartida.jsp");
	}else{
		response.sendRedirect("partida.jsp?idPartida="+partida.getId());
	}
%>
