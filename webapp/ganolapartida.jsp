<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page session="true" %>
<%
	if(session.getAttribute("dni1") == null | session.getAttribute("dni2") == null){
		response.sendRedirect("index.jsp");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gan�</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="page-header">
			<h1>Felicitaciones <%=session.getAttribute("ganador").toString() %>, ganaste la partida.</h1>
		</div>

	<div class="col-sm-3">
		<div class="list-group">
			<a class="list-group-item" href="partidas.jsp">Ir al listado de partidas</a>
			<a class="list-group-item" href="cerrarSesion.jsp">Cerrar Sesi�n</a>
		</div>
	</div>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>