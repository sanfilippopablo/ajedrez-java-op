<%@ page 
	session="true"
	import="negocio.ListaPartidasCtrl"
	import="negocio.InicioCtrl"
	import="entidades.Jugador"
	import="java.util.ArrayList"
%>
<%
	session = request.getSession();
	
	if(session.getAttribute("dni1") == null & session.getAttribute("dni2") == null){
		response.sendRedirect("index.jsp");
	}
%>
<%
	ListaPartidasCtrl listaPartidasCtrl = new ListaPartidasCtrl();
	InicioCtrl inicioCtrl = new InicioCtrl();
	ArrayList<Jugador> jugadores = inicioCtrl.getJugadores(session.getAttribute("dni1").toString(), session.getAttribute("dni2").toString());
	int idPartida = listaPartidasCtrl.iniciarNuevaPartida(jugadores.get(0), jugadores.get(1));
	response.sendRedirect("partida.jsp?idPartida="+idPartida);
%>