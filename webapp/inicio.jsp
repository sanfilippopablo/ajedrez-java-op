<%@ page errorPage="index.jsp" %>
<%@ page 
	session="true"
	import="negocio.InicioCtrl"
	import="entidades.Jugador"
	import="entidades.Partida"
	import="java.util.ArrayList"
	import="errores.ValidationError"	
%>
<%
	session = request.getSession();
	ArrayList<Jugador> jugadores=new ArrayList<Jugador>();
	String dniText1 = "";
	String dniText2 = "";
	InicioCtrl inicioCtrl = new InicioCtrl();
	
	if(session.getAttribute("dni1") != null & session.getAttribute("dni2") != null){
		dniText1 = (String)session.getAttribute("dni1").toString();
		dniText2 = (String)session.getAttribute("dni2").toString();	
	}else{
		if(request.getParameter("dni1") != null & request.getParameter("dni2") != null){
			dniText1 = request.getParameter("dni1").toString();
			dniText2 = request.getParameter("dni2").toString();	
		}
	}

	jugadores = inicioCtrl.getJugadores(dniText1, dniText2);

	if(!jugadores.isEmpty()){
		session.setAttribute( "dni1", jugadores.get(0).getdNI());
		session.setAttribute("dni2",jugadores.get(1).getdNI());
		session.setAttribute("jugador1",jugadores.get(0).getApellido());
		session.setAttribute("jugador2",jugadores.get(1).getApellido());
		response.sendRedirect("partidas.jsp");
	}else{
		response.sendRedirect("index.jsp");
	}
%>