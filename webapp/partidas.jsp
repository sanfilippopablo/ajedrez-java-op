
<%@ page isErrorPage="true" import="java.util.*" %>
<%@ page
    import="negocio.InicioCtrl"
    import="java.util.ArrayList"
    import="entidades.Partida"
    import="errores.ValidationError"
	session="true"
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%	
	session = request.getSession();
	if(session == null){
		response.sendRedirect("index.jsp");
	}
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Partidas</title>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<div class="page-header">
				<h1>Partidas entre <%=(String)session.getAttribute("jugador1")%> y <%=(String)session.getAttribute("jugador2")%></h1>
			</div>
			
			<div class="row">
				<div class="list-group col-md-2">
<%			
			InicioCtrl inicioCtrl = new InicioCtrl();
			ArrayList<Partida> partidas = inicioCtrl.iniciar(session.getAttribute("dni1").toString(), session.getAttribute("dni2").toString());

			for(Partida partida: partidas){
%>
					<a class="list-group-item" href="partida.jsp?idPartida=<%=partida.getId()%>"><%=partida.getId()%></a>
<% 
			}
%>
				</div>
				<div class="col-md-2">
					<div class="list-group">
						<a class="list-group-item" href="nuevaPartida.jsp">Iniciar nueva partida</a>
						<a class="list-group-item" href="cerrarSesion.jsp">Cerrar Sesión</a>
					</div>
				</div>
			</div>
<% 			
			if(exception != null & exception instanceof ValidationError){
%>	
				<p>
					<%=exception.getMessage()%>
				</p>	
<%
			} 
%>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</body>
</html>