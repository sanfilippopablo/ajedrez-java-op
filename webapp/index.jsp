<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ page isErrorPage="true" 
	import="java.util.*"
	import="errores.ValidationError" 
%>
<%
	session = request.getSession();
	if(session.getAttribute("dni1") != null & session.getAttribute("dni2") != null){
		response.sendRedirect("inicio.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Ajedrez</title>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	</head>
	<body>

		<div class="container">
			<div class="page-header">
				<h1>Entrar al juego</h1>			
			</div>

		<form class="form-horizontal" action="inicio.jsp" method="post">
<% 			
			if(exception != null & exception instanceof ValidationError){
%>	
				<div class="form-group" style="color: red">
					<div class="col-md-offset-2">
						<p class="help-text"><%= exception.getMessage() %></p>
					</div>
				</div>	
<%
			} 
%>
			<div class="form-group">
				<label class="control-label col-md-2" for="dni1">DNI 1</label>
				<div class="col-md-2">
					<input type="text" name="dni1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2" for="dni2">DNI 2</label>
				<div class="col-md-2">
					<input type="text" name="dni2" class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
			    	<button type="submit" class="btn btn-primary">Sign in</button>
			    </div>
			</div>

			<input type="hidden" name="accion" value="ingresar" />
		</form>

		
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	</body>
</html>